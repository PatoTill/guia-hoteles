$(function () {
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();
	$('.carousel').carousel({
		interval: 3000
	});


	$("#boletín").on('show.bs.modal', function (e){
		console.log('El modal se esta mostrando');
		$('#masinfoBtn').removeClass('btn-outline-success');
		$('#masinfoBtn').addClass('btn-primary');
		$('#masinfoBtn').prop('disabled', true);
	});
	$("#boletín").on('shown.bs.modal', function (e){
		console.log('El modal se mostró, el botón se inhabilitó y cambió el color');
	});
	$("#boletín").on('hide.bs.modal', function (e){
		console.log('El modal se esta ocultando');
	});
	$("#boletín").on('hidden.bs.modal', function (e){
		console.log('El modal se ocultó, el botón se habilitó y volvió al color original');
		$('#masinfoBtn').prop('disabled', false);
		$('#masinfoBtn').removeClass('btn-primary');
		$('#masinfoBtn').addClass('btn-outline-success');
	});
					
});